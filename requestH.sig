signature REQUESTH_IN =
sig
    val table : string
    val adminGroup : string
    val subject : string -> string
    val body : {node : int, mail : Mail.session, data : string} -> unit
    val template : string
    val descr : string
end

signature REQUESTH_OUT =
sig
    datatype status =
	     NEW
	   | INSTALLED
	   | REJECTED

    type request = { id : int, usr : int, node : int, data : string, msg : string, status : status,
		     stamp : Init.C.timestamp, cstamp : Init.C.timestamp option }

    val statusFromInt : int -> status

    val add : {usr : int, node : int, data : string, msg : string} -> int
    val lookup : int -> request
    val modify : request -> unit
    val delete : int -> unit
    val list : unit -> (string * request) list
    val listOpen : unit -> (string * request) list

    val notifyNew : int -> bool
    val notifyMod : {old : status, new : status, changer : string, req : int} -> bool
end
