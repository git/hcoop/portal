signature QOS = sig

    type entry = { kind : string, kindUrl : string option, name : string, url : string option, usr : int, uname : string,
		   stamp : Init.C.timestamp, pstamp : Init.C.timestamp option, cstamp : Init.C.timestamp option}

    val recent : int -> entry list

    type grade = { count : int, minutes : int }
    type grades = { pending : grade, closed : grade }
    type reportCard = { misc : grades,
			apt : grade,
			domain : grade,
			mailingList : grade,
			sec : grade }

    val reportCard : int -> reportCard

end
