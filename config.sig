signature CONFIG = sig

val scratchDir : string
val urlPrefix : string
val emailSuffix : string
val boardEmail : string
val dbstring : string
val kerberosSuffix : string
val passgenDbstring : string
val statsRoot : string
val staticFilesRoot : string

val joinBannedEmailDomains : string list
(* domains that may not be used in member applications, really
rudimentary spam filtering for some persistent spammers that use the
same two domains. *)

end
