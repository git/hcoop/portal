signature LINK =
sig
    type link = {id : int, usr : int, title : string, url : string, descr : string}

    val lookupLink : int -> link
    val listLinks : unit -> (string * link) list
    val listUserLinks : int -> link list
    val addLink : int * string * string * string -> int
    val modLink : link -> unit
    val deleteLink : int -> unit
end