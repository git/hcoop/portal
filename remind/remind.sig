signature REMIND =
sig
    val main : string * string list -> OS.Process.status
end
