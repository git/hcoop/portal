# -*- python -*-
# Copyright (C) 2014 by Clinton Ebadi <clinton@unknownlamer.org>

import sys
sys.path.insert(0, '/afs/hcoop.net/user/h/hc/hcoop/portal3/stripe/stripe-pkg/lib/python2.6/site-packages/')

import stripe, cgi, psycopg2, cgitb, datetime, smtplib
from email.mime.text import MIMEText
from contextlib import contextmanager

def notify_payment (charge, member):
    msg_text = """
A member has paid us via Stripe. Please visit the portal money
page at your earliest convenience to process the payment.

  Member          : {0}
  Amount (cents)  : {1}
  Stripe Charge ID: {2}
""".format (member, charge.amount, charge.id)

    msg = MIMEText(msg_text)
    msg['Subject'] = 'Stripe payment received from {0}'.format(member)
    msg['From'] = 'payment@hcoop.net'
    msg['To'] = 'payment@hcoop.net'

    s = smtplib.SMTP ('mail-ipv4.hcoop.net')
    s.sendmail ('payment@hcoop.net', ['payment@hcoop.net'], msg.as_string ())
    s.quit ()

def notify_payment_rejected (charge, reason):
    # TODO: notify member...
    msg_text = """We have rejected a payment from a member.
  Amount:           {0}
  Stripe Charge ID: {1}
  Reason: {2}
""".format (charge.amount, charge.id, reason)

    msg = MIMEText(msg_text)
    msg['Subject'] = 'Stripe payment rejected'
    msg['From'] = 'payment@hcoop.net'
    msg['To'] = 'payment@hcoop.net'

    s = smtplib.SMTP ('mail.hcoop.net')
    s.sendmail ('payment@hcoop.net', ['payment@hcoop.net'], msg.as_string ())
    s.quit ()

def stripe_key ():
    keyfile = open ("/afs/hcoop.net/user/h/hc/hcoop/.portal-private/stripe", "r")
    keystring = keyfile.read ()
    keyfile.close ()
    return keystring

@contextmanager
def stripe_error_handling ():
    try:
        yield
    except stripe.error.CardError, e: # The card has been declined
        print 'Status: 200 OK'
        print
        print '<html>'
        print '<head><title>Transaction Failed</title></head>'
        print '<body>'
        print '<h1>Failed</h1><p>Reason: '
        print e.json_body['error']['message']
        print '</p>'
        print '</body>'
        print '</html>'
        raise
    except stripe.error.StripeError, e: # General stripe failure
        print 'Status: 200 OK'
        print
        print '<html>'
        print '<head><title>Stripe Error</title></head>'
        print '<body>'
        print '<h1>Failed</h1><p>Reason: '
        print e.json_body['error']['message']
        print '</p>'
        print '</body>'
        print '</html>'
        raise

@contextmanager
def stripe_refund_on_error (charge):
    try:
        yield
    except:
        print 'Status: 200 OK'
        print 'Content-Type: text/html'
        print ''
        print '<h1>Something went wrong after accepting payment!</h1>'
        charge.refund ()
        print '<p>The charge should be refunded. Please contact payment@hcoop.net if it was not!</p>'
        raise

def stripe_success (redirect_to):
    print 'Status: 303 See Other'
    print 'Location: {0}'.format(redirect_to);
    print ''
    print '<a href="{0}">Go back to the portal</a>'.format(redirect_to)

connstring = 'dbname=hcoop_portal3 user=hcoop host=postgres port=5433'

def hcoop_stripe_init ():
    cgitb.enable()
    stripe.api_key = stripe_key ()
