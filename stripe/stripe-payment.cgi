#!/usr/bin/env python
# -*- python -*-

from hcoopstripe import *

import stripe, cgi, psycopg2, cgitb, datetime, smtplib

hcoop_stripe_init ()

# Get the credit card details submitted by the form

request_params = cgi.FieldStorage()
request_command = request_params.getvalue ('cmd', 'none');

assert request_command != 'none', 'No command given.'

# Create the charge on Stripe's servers - this will charge the user's card 

if request_command == 'member_payment':
    token = request_params.getvalue ('stripeToken')
    webuser_id = request_params.getvalue('webuser_id')
    member_name = request_params.getvalue('webuser_name')
    amount = request_params.getvalue('stripeDues')

    with stripe_error_handling ():
        charge = stripe.Charge.create( amount=amount,
                                       currency="usd", 
                                       card=token, 
                                       description='Payment for member {0}'.format (member_name))

    with stripe_refund_on_error (charge):
#        assert charge.card.address_line1_check == 'pass', 'Address verification failed or unknown.'
        assert charge.card.cvc_check != 'fail', 'CVC verification failed.'
#        assert charge.card.address_zip_check == 'pass', 'Zipcode verification failed or unknown.'

        balance = stripe.BalanceTransaction.retrieve (charge.balance_transaction)
        conn =  psycopg2.connect ('dbname=hcoop_portal3 user=hcoop host=postgres port=5433')
        cur = conn.cursor ()
        cur.execute ('insert into stripe_payment (charge_id, card_name, webuser_id, paid_on, gross, fee) values (%s, %s, %s, %s, %s, %s)', 
                     (charge.id, charge.card.name, webuser_id, datetime.date.today (), charge.amount, balance.fee))
        conn.commit ()

    notify_payment (charge, member_name)
    stripe_success ('/portal/portal?cmd=stripeSuccess')
else:
    assert False, 'Invalid command.'

# Use mod_authz_groupfile to store money/root
#   (All hcoop members should be able to use this!)
#  [support Satisfy? Satisfy: all is OK for now...]
# Whenever groups are updated in the portal, write the file
# make sure to store the file outside of the web root (duh)
# only users in money/root can do reject/adduser
# common code should go into a module (feh!)
# application_payment in one cgi (anyone)
# member_payment in another (only kerberos users)
# reject_payment / capture_application_payment (kerberos + inGroup {money, root})

# If there is a way to allow all and check the group info
# here... maybe investigate, but beware security holes
# alt: libapache2-mod-authnz-external + db helper script
# can use ExternalGroup, check kerberos user is in group specified in
# another env var

