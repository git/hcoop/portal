#!/usr/bin/env python
# -*- python -*-

# Copyright (C) 2014 by Clinton Ebadi <clinton@unknownlamer.org>

from hcoopstripe import *
import stripe, cgi, psycopg2, cgitb, datetime

hcoop_stripe_init ()

request_params = cgi.FieldStorage()
request_command = request_params.getvalue ('cmd', 'none');

assert request_command != 'none', 'No command given.'

hcoop_stripe_init ()

if request_command == 'reject_member_payment':
    charge_id = request_params.getvalue ('stripeChargeId');
    reason = request_params.getvalue ('reason', 'none given');
    with stripe_error_handling ():
        conn = psycopg2.connect (connstring)
        cur = conn.cursor ()

        cur.execute ('SELECT charge_id FROM stripe_payment WHERE charge_id = %s', (charge_id,))
        assert cur.fetchone() != None, 'Bad charge id'
        cur.execute ('SELECT stripe_charge_id FROM stripe_handled WHERE stripe_charge_id = %s', (charge_id,))
        assert cur.fetchone() == None, 'Cannot rejected a previously handled payment'
        
        charge = stripe.Charge.retrieve (charge_id);
        charge.refund ()
        cur.execute ('insert into stripe_rejected (stripe_charge_id, refunded_on, reason) values (%s, %s, %s)',
                     (charge.id, datetime.date.today (), reason))
        conn.commit ()

    notify_payment_rejected (charge, reason)
    stripe_success ('/portal/money?cmd=stripeRejected')
else:
    assert False, 'Invalid command.'
