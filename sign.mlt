<% @header [("title", ["SSL certificate signing requests"])];

val admin = Group.inGroupName "server";

if $"new" <> "" then
   val req = $"req";
   val key = $"key";
   val days = Web.stoi ($"days");
   val domain = $"domain";
   val msg = $"msg";

   if req = "" then
      %><h3>Please fill in a path to a certificate request.</h3><%
   elseif key = "" then
      %><h3>Please fill in a path to a key.</h3><%
   elseif days <= 0 then
      %><h3>Please give a positive number of days for the certificate to live.</h3><%
   elseif not (ChooseDomain.yourDomain {user = Init.getUserName (), domain = domain}) then
      %><h3>You don't have permissions on domain <tt><% Web.html domain %></tt>.</h3><%
   else
      %>Are you sure you want to ask for permissions on an SSL certificate
      <li> with request <tt><% Web.html req %></tt>,</li>
      <li> with key <tt><% Web.html key %></tt>,</li>
      <li> for domain <tt><% domain %></tt>,</li>
      <li> lasting <% days %> days?</li>
      <a href="?cmd=request&req=<% Web.html req %>&key=<% Web.html key %>&days=<% days %>&domain=<% Web.html domain %>&msg=<% Web.urlEncode msg %>">Yes, I want to request that.</a><%
   end
elseif $"cmd" = "request" then
   val req = $"req";
   val key = $"key";
   val days = Web.stoi ($"days");
   val domain = $"domain";
   val msg = $"msg";

   if req = "" then
      %><h3>Please fill in a path to a certificate request.</h3><%
   elseif key = "" then
      %><h3>Please fill in a path to a key.</h3><%
   elseif days <= 0 then
      %><h3>Please give a positive number of days for the certificate to live.</h3><%
   elseif not (ChooseDomain.yourDomain {user = Init.getUserName (), domain = domain}) then
      %><h3>You don't have permissions on domain <tt><% Web.html domain %></tt>.</h3><%
   else
     val gen = req;

     val text = "/afs/hcoop.net/common/etc/scripts/ca-sign "
       ^ Int.toString days ^ " "
       ^ req ^ " "
       ^ key ^ " "
       ^ gen ^ " "
       ^ domain;

     val id = Sign.add (Init.getUserId (), text, msg);
     if not (Sign.notifyNew id) then
	%><h3>Error sending e-mail notification</h3><%
     end
     %><h3>Request added</h3><%
  end
elseif $"cmd" = "open" then
	%><h3>Open requests</h3>
	<a href="?cmd=list">List all requests</a><%

	foreach (name, req) in Sign.listOpen () do %>
<br><hr><br>
<table class="blanks">
<tr> <td>By:</td> <td><a href="user?id=<% #usr req %>"><% name %></a></td> </tr>
<tr> <td>Time:</td> <td><% #stamp req %> (<% Util.diffFromNow (#stamp req) %> ago)</td> </tr>
<tr> <td>Request:</td> <td><tt><% #data req %></tt></td> </tr>
<tr> <td>Reason:</td> <td><% Web.html (#msg req) %></td> </tr>
</table>

<% if admin then %>
	<br>
	<a href="?mod=<% #id req %>">[Modify]</a>
	<a href="?del=<% #id req %>">[Delete]</a><br>
<% end %>

<%	end

elseif $"cmd" = "list" then
	%><h3>All requests</h3><%

	foreach (name, req) in Sign.list () do %>
<br><hr><br>
<table class="blanks">
<tr> <td>By:</td> <td><a href="user?id=<% #usr req %>"><% name %></a></td> </tr>
<tr> <td>Time:</td> <td><% #stamp req %> (<% Util.diffFromNow (#stamp req) %> ago)</td> </tr>
<tr> <td>Request:</td> <td><tt><% #data req %></tt></td> </tr>
<tr> <td>Reason:</td> <td><% Web.html (#msg req) %></td> </tr>
</table>

<% if admin then %>
	<br>
	<a href="?mod=<% #id req %>">[Modify]</a>
	<a href="?del=<% #id req %>">[Delete]</a>
<% end %>

<%	end

elseif $"mod" <> "" then
	Group.requireGroupName "server";
	val id = Web.stoi ($"mod");
	val req = Sign.lookup id;
	val user = Init.lookupUser (#usr req) %>
<h3>Handle request</h3>

<form method="post">
<input type="hidden" name="save" value="<% id %>">
<table class="blanks">
<tr> <td>Requestor:</td> <td><a href="user?id=<% #usr req %>"><% #name user %></a></td> </tr>
<tr> <td>Time:</td> <td><% #stamp req %> (<% Util.diffFromNow (#stamp req) %> ago)</td> </tr>
<tr> <td>Status:</td> <td><select name="status">
	<option value="0"<% if #status req = Sign.NEW then %> selected<% end %>>New</option>
	<option value="1"<% if #status req = Sign.INSTALLED then %> selected<% end %>>Installed</option>
	<option value="2"<% if #status req = Sign.REJECTED then %> selected<% end %>>Rejected</option>
</select></td> </tr>
<tr> <td>Request:</td> <td><input name="req" size="60" value="<% Web.html (#data req) %>"></td> </tr>
<tr> <td>Message:</td> <td><textarea name="msg" rows="10" cols="80" wrap="soft"><% Web.html (#msg req) %></textarea></td> </tr>
<tr> <td><input type="submit" value="Save"></td> </tr>
</table>
</form>

<% elseif $"save" <> "" then
	Group.requireGroupName "server";
	val id = Web.stoi ($"save");
	val req = Sign.lookup id;
	val oldStatus = #status req;
	val newStatus = Sign.statusFromInt (Web.stoi ($"status"));
	Sign.modify {req with data = $"req", msg = $"msg", status = newStatus};
	if not (Sign.notifyMod (oldStatus, newStatus, Init.getUserName(), id)) then
		%><h3>Error sending e-mail notification</h3><%
	end
	%><h3>Request modified</h3>
	Back to: <a href="?cmd=open">open requests</a>, <a href="?cmd=list">all requests</a>

<% elseif $"del" <> "" then
	Group.requireGroupName "server";
	val id = Web.stoi ($"del");
	val req = Sign.lookup id;
	val user = Init.lookupUser (#usr req)
	%><h3>Are you sure you want to delete request by <% #name user %> for <tt><% #data req %></tt>?</h3>
	<a href="?del2=<% id %>">Yes, I'm sure!</a>

<% elseif $"del2" <> "" then
	Group.requireGroupName "server";
	val id = Web.stoi ($"del2");
	Sign.delete id
	%><h3>Request deleted</b><h3>
	Back to: <a href="?cmd=open">open requests</a>, <a href="?cmd=list">all requests</a>

<% else %>

<h3>Request SSL certificate signing</h3>

<p>Use this form to request that we sign an an SSL certificate with our certificate authority.  Give the location of your certificate request (<tt>.csr</tt> file) and key (<tt>.key</tt> file) within <tt>/afs/hcoop.net</tt>.</p>

<p>The <a href="http://wiki.hcoop.net/MemberManual/ServingWebsites/SslCert">instructions on our wiki for creating SSL certificates</a> may be helpful.</p>

<form method="post">
<input type="hidden" name="new" value="1">
<table class="blanks">
<tr> <td>Domain:</td> <td><% @chooseDomain[] %></td> </tr>
<tr> <td>OpenSSL request file:</td> <td><input name="req" size="60"></td> </tr>
<tr> <td>OpenSSL key file:</td> <td><input name="key" size="60"></td> </tr>
<tr> <td>Certificate lifetime, in days:</td> <td><input name="days" size="5" value="3650"></td></tr>
<tr> <td>Additional comments:</td> <td><textarea name="msg" rows="5" cols="80" wrap="soft"></textarea></td> </tr>
<tr> <td><input type="submit" value="Request"></td> </tr>
</table>
</form>

<% end %>

<% @footer[] %>
