structure Cert = Request(struct
			 val table = "Cert"
			 val adminGroup = "server"
			 fun subject _ = "SSL certificate installation request"
			 val template = "cert"
			 val descr = "SSL certificate"
				      
			 fun body (mail, data) =
			     (Mail.mwrite (mail, " Request: ");
			      Mail.mwrite (mail, data);
			      Mail.mwrite (mail, "\n"))
			 end)
