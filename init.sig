signature INIT = sig
    structure C : SQL_CLIENT
    val nullableFromSql : (C.value -> 'a) -> C.value -> 'a option
    val nullableToSql : ('a -> string) -> 'a option -> string

    val scratchDir : string
    val urlPrefix : string
    val boardEmail : string

    exception Access of string
    exception NeedTos

    val emailSuffix : string

    type user = {id : int, name : string, rname : string, bal : int, joined : C.timestamp,
		 app : int, shares : int, paypal : string option, checkout : string option }

    val rowError : string * C.value list -> 'a

    val mkUserRow : C.value list -> user

    (* Direct access to database connections *)
    val conn : unit -> C.conn

    (* Open or close a session, wrapped in a transaction *)
    val init : unit -> unit
    val done : unit -> unit

    val nextSeq : C.conn * string -> int

    (* Fun with users *)

    val lookupUser : int -> user
    val listUsers : unit -> user list
    val addUser : string * string * int * int * int -> int
    (* Pass name, real name, balance ID, and share count *)
    val modUser : user -> unit
    val deleteUser : int -> string
    val byPledge : unit -> user list

    val validUsername : string -> bool
    val userNameToId : string -> int option

    val getDb : unit -> C.conn

    val getUser : unit -> user
    val getUserId : unit -> int
    val getUserName : unit -> string

    val dateString : unit -> string

    type node = {id : int, name : string, descr : string, debian : string}

    val listNodes : unit -> node list
    val nodeName : int -> string
    val nodeDebian : int -> string

    val explain : exn -> string
    val tokens : unit -> string
    val tokensForked : unit -> unit

    val usersDiff : string list * string list ->
		    {onlyInFirst : string list, onlyInSecond : string list}
    val listUsernames : unit -> string list
    val usersInAfs : unit -> string list

    val searchPaypal : string -> user list
    val searchCheckout : string -> user list
    val searchRealName : string -> user list
end
