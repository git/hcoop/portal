structure Apt = RequestH(struct
			 val table = "Apt"
			 val adminGroup = "server"
			 fun subject _ = "Apt package installation request"
			 val template = "apt"
			 val descr = "packages"

			 fun body {node, mail, data = pkgs} =
			     let
				 val pkgs = String.tokens Char.isSpace pkgs
				 val infos = map (valOf o (fn x => AptQuery.query {node = node, pkg = x})) pkgs

				 fun rightJustify (n, s) =
				     let
					 fun pad n =
					     if n <= 0 then
						 ()
					     else
						 (Mail.mwrite (mail, " ");
						  pad (n-1))
				     in
					 pad (n - size s);
					 Mail.mwrite (mail, s)
				     end
			     in
				 app (fn info =>
					 (rightJustify (10, #name info);
					  Mail.mwrite (mail, "  ");
					  Mail.mwrite (mail, #descr info);
					  Mail.mwrite (mail, "\n"))) infos
			     end
			 end)
