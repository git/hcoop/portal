<% val catId = Web.stoi ($"cat");
val cat = Support.lookupCategory catId;
val admin = Group.inGroupNum (#grp cat);
val you = Init.getUserId()

ref viewingIssue = NONE;

@header[("title", ["Support: " ^ Web.html (#name cat)])];

ref showNormal = true;

if $"cmd" = "new" then
	showNormal := false %>

<h3>New issue</h3>

<form action="issue" method="post">
<input type="hidden" name="cat" value="<% catId %>">
<input type="hidden" name="cmd" value="add">
<table class="blanks">
<tr> <td>Title:</td> <td><input name="title"></td> </tr>
<tr> <td><input type="checkbox" name="priv"></td> <td>Only make this issue accessible to the admins for this support category.</td> </tr>
<tr> <td>Description:</td> <td><textarea name="body" rows="10" cols="80" wrap="soft"></textarea></td> </tr>
<tr> <td><input type="submit" value="Add"></td> </tr>
</table>
</form>

<% elseif $"cmd" = "add" then
	val title = $"title";
	if not (Support.validTitle title) then
		%><h3>Invalid title</h3><%
	else
		val id = Support.addIssue (you, catId, title, $"priv" = "on", Support.NEW);
		val _ = Support.addPost (you, id, $"body");
		if not (Support.notifyCreation id) then
			%><h3>Error sending e-mail notification</h3><%
		end;
		viewingIssue := SOME id
	end

elseif $"cmd" = "list" then
	showNormal := false %>
	<h3>All issues</h3>

<table>
<% foreach (name, issue) in Support.listCategoryIssues catId do
	if (iff admin then true else (iff #priv issue then (#usr issue = you) else true)) then %>
	<tr> <td><a href="issue?cat=<% catId %>&id=<% #id issue %>"><% Web.html (#title issue) %></a>
<% if #priv issue then %><i>(private)</i><% end %>
	</td>
	<td><a href="user?id=<% #usr issue %>"><% name %></a></td>
	<td><% #stamp issue %></td>
	<td><% switch #status issue of
			  Support.NEW => %>New<%
			| Support.PENDING => %>Pending<%
			| Support.CLOSED => %>Closed<%
		end %></td> </tr>
<% end
end %>
</table>

<% elseif $"mod" <> "" then
	showNormal := false;
	val id = Web.stoi ($"mod");
	val issue = Support.lookupIssue id;
	if catId <> #cat issue then
		%><h3>Inconsistent cat field</h3><%
	elseif not admin then
		%><h3>You aren't authorized to modify that.</h3><%
	else %>
<h3>Modify issue</h3>

<form action="issue" method="post">
<input type="hidden" name="cat" value="<% catId %>">
<input type="hidden" name="save" value="<% id %>">
<table class="blanks">
<tr> <td>Category:</td> <td><select name="newCat">
<% foreach cat in Support.listCategories () do %>
	<option value="<% #id cat %>"<% if #id cat = catId then %> selected<% end %>><% Web.html (#name cat) %></option>
<% end %></select></td> </tr>
<tr> <td>Title:</td> <td><input name="title" value="<% Web.html (#title issue) %>"></td> </tr>
<tr> <td><input type="checkbox" name="priv"<% if #priv issue then %> checked<% end %>></td> <td>Only make this issue accessible to the admins for this support category.</td> </tr>
<tr> <td>Status:</td> <td><select name="status">
	<option value="0"<% if #status issue = Support.NEW then %> selected<% end %>>New</option>
	<option value="1"<% if #status issue = Support.PENDING then %> selected<% end %>>Pending</option>
	<option value="2"<% if #status issue = Support.CLOSED then %> selected<% end %>>Closed</option>
</select></td> </tr>
<tr> <td><input type="submit" value="Save"></td> </tr>
</table>
</form>
<%	end

elseif $"save" <> "" then
	val id = Web.stoi ($"save");
	val issue = Support.lookupIssue id;
	val title = $"title";
	val oldStatus = #status issue;
	val status = Web.stoi ($"status");
	val newCat = Support.lookupCategory (Web.stoi ($"newCat"));

	if catId <> #cat issue then
		%><h3>Inconsistent cat field</h3><%
	elseif (iff admin then not (Group.inGroupNum (#grp newCat)) else false) then
		%><h3>Authorization failure</h3><%
	elseif not (Support.validTitle title) then
		%><h3>Invalid title</h3><%
	elseif (iff status < 0 then false else status > 2) then
		%><h3>Invalid status</h3><%
	else
		val status = (case status of
				  0 => Support.NEW
				| 1 => Support.PENDING
				| _ => Support.CLOSED);

		Support.modIssue {issue with cat = #id newCat, title = title,
				priv = ($"priv" = "on"),
				status = status};
		if status <> oldStatus then
			if not (Support.notifyStatus (you, oldStatus, status, id)) then
				%><h3>Error sending e-mail notification</h3><%
			end
		end;
		viewingIssue := SOME id
		%><h3>Issue saved</b></h3<%
	end

elseif $"del" <> "" then
	showNormal := false;
	val id = Web.stoi ($"del");
	val issue = Support.lookupIssue id;

	if catId <> #cat issue then
		%><h3>Inconsistent cat field</h3><%
	elseif not admin then
		%><h3>Authorization failure</h3><%
	else
		%><h3>Are you sure you want to delete "<% Web.html (#title issue) %>"?</h3>
		<a href="issue?cat=<% catId %>&del2=<% id %>">Yes, delete "<% Web.html (#title issue) %>"!</a><%
	end

elseif $"del2" <> "" then
	val id = Web.stoi ($"del2");
	val issue = Support.lookupIssue id;

	if catId <> #cat issue then
		%><h3>Inconsistent cat field</h3><%
	elseif not admin then
		%><h3>Authorization failure</h3><%
	else
		Support.deleteIssue id
		%><h3>Issue "<% Web.html (#title issue) %>" deleted</h3><%
	end

elseif $"cmd" = "post" then
	val id = Web.stoi ($"iss");
	viewingIssue := SOME id;
	val issue = Support.lookupIssue id;

	if catId <> #cat issue then
		%><h3>Inconsistent cat field</h3><%
	elseif not (Support.allowedToSee id) then
		%><h3>Authorization failure</h3><%
	else
		val id = Support.addPost (you, id, $"body");
		if not (Support.notifyPost id) then
			%><h3>Error sending e-mail notification</h3><%
		end
		%><h3>Posted</h3><%
	end

elseif $"modPost" <> "" then
	showNormal := false;
	val id = Web.stoi ($"modPost");
	val post = Support.lookupPost id;
	val issue = Support.lookupIssue (#iss post);
	if catId <> #cat issue then
		%><h3>Inconsistent cat field</h3><%
	elseif not admin then
		%><h3>You aren't authorized to modify that.</h3><%
	else %>
<h3>Modify post</h3>

<form action="issue" method="post">
<input type="hidden" name="cat" value="<% catId %>">
<input type="hidden" name="savePost" value="<% id %>">
<textarea name="body" rows="10" cols="80" wrap="soft"><% Web.htmlNl (#body post) %></textarea>
<input type="submit" value="Save">
</form>
<%	end

elseif $"savePost" <> "" then
	val id = Web.stoi ($"savePost");
	val post = Support.lookupPost id;
	val issue = Support.lookupIssue (#iss post);

	if catId <> #cat issue then
		%><h3>Inconsistent cat field</h3><%
	elseif not admin then
		%><h3>Authorization failure</h3><%
	else
		Support.modPost {post with body = $"body"};
		viewingIssue := SOME (#iss post)
		%><h3>Post saved</b></h3<%
	end

elseif $"delPost" <> "" then
	showNormal := false;
	val id = Web.stoi ($"delPost");
	val post = Support.lookupPost id;
	val issue = Support.lookupIssue (#iss post);

	if catId <> #cat issue then
		%><h3>Inconsistent cat field</h3><%
	elseif not admin then
		%><h3>Authorization failure</h3><%
	else
		%><h3>Are you sure you want to delete this post?</h3>
		<blockquote><% Web.htmlNl (#body post) %></blockquote>
		<a href="issue?cat=<% catId %>&delPost2=<% id %>">Yes, delete it!</a><%
	end

elseif $"delPost2" <> "" then
	val id = Web.stoi ($"delPost2");
	val post = Support.lookupPost id;
	val issue = Support.lookupIssue (#iss post);

	if catId <> #cat issue then
		%><h3>Inconsistent cat field</h3><%
	elseif not admin then
		%><h3>Authorization failure</h3><%
	else
		Support.deletePost id;
		viewingIssue := SOME (#iss post)
		%><h3>Post deleted</h3><%
	end

elseif $"id" <> "" then
	viewingIssue := SOME (Web.stoi ($"id"))
end;

switch viewingIssue of
	  SOME id => 
		val issue = Support.lookupIssue id;
		val canEdit = Support.allowedToEdit id;
		val canView = Support.allowedToSee id;
		if catId <> #cat issue then
			%><h3>Inconsistent cat field</h3><%
		elseif not canView then
			%><h3>You aren't authorized to view that.</h3><%
		else
			val user = Init.lookupUser (#usr issue) %>

<table class="blanks">
<tr> <td>Title:</td> <td><% Web.html (#title issue) %></td> </tr>
<tr> <td>Created by:</td> <td><a href="user?id=<% #usr issue %>"><% #name user %></a></td> </tr>
<tr> <td>At:</td> <td><% #stamp issue %></td> </tr>
<tr> <td>Private:</td> <td><% if #priv issue then %>yes<% else %>no<% end %></td> </tr>
<tr> <td>Status:</td> <td><% switch #status issue of
	  Support.NEW => %>New<%
	| Support.PENDING => %>Pending<%
	| Support.CLOSED => %>Closed<%
	end %></td> </tr>
<%	switch #pstamp issue of
		  SOME stamp =>	%><tr> <td>Changed to pending:</td> <td><% stamp %></td> </tr><%
	end;
	switch #cstamp issue of
		  SOME stamp =>	%><tr> <td>Closed:</td> <td><% stamp %></td> </tr><%
	end %>
</table>

<% if admin then %>
<a href="issue?cat=<% catId %>&mod=<% id %>">Modify this issue</a><br>
<a href="issue?cat=<% catId %>&del=<% id %>">Delete this issue</a><br>
<% end;

foreach (name, post) in Support.listPosts id do %>
<br><hr><br>
<a href="user?id=<% #usr post %>"><% name %></a> at <% #stamp post %>:
<% if admin then %>
<a href="issue?cat=<% catId %>&modPost=<% #id post %>">[Modify]</a>
<a href="issue?cat=<% catId %>&delPost=<% #id post %>">[Delete]</a>
<% end %>

<p><% Web.htmlNl (#body post) %></p>

<% end %>

<br><hr><br>

<h3>Post to this thread</h3>

<form action="issue" method="post">
<input type="hidden" name="cat" value="<% catId %>">
<input type="hidden" name="iss" value="<% id %>">
<input type="hidden" name="cmd" value="post">
<textarea name="body" rows="10" cols="80"></textarea><br>
<input type="submit" value="Post">
</form>

<% end
		
	| NONE =>
if showNormal then %>

<a href="issue?cat=<% catId %>&cmd=new">New issue</a><br>
<a href="issue?cat=<% catId %>&cmd=list">List all issues (including closed issues)</a>

<h3>Open issues</h3>

<table>
<% val issues = iff admin then Support.listOpenCategoryIssuesAdmin catId
			else Support.listOpenCategoryIssues (catId, you);

foreach (name, issue) in issues do %>
	<tr> <td><a href="issue?cat=<% catId %>&id=<% #id issue %>"><% Web.html (#title issue) %></a>
<% if #priv issue then %><i>(private)</i><% end %>
	</td>
	<td><a href="user?id=<% #usr issue %>"><% name %></a></td>
	<td><% #stamp issue %></td>
	<td><% switch #status issue of
			  Support.NEW => %>New<%
			| Support.PENDING => %>Pending<%
			| Support.CLOSED => %>Closed<%
		end %></td> </tr>
<% end %>

<% end
end %>

<% @footer[] %>