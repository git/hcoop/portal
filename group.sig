signature GROUP =
sig
    type group = {id :int, name : string}
    type membership = {usr : int, grp : int}

    val addGroup : string -> int
    val lookupGroup : int -> group
    val modGroup : group -> unit
    val deleteGroup : int -> unit
    val listGroups : unit -> group list

    val validGroupName : string -> bool
    val groupNameToId : string -> int option

    val userInGroupNum : int * int -> bool
    val userInGroupName : int * string -> bool
    val userReallyInGroupName : int * string -> bool (* Doesn't count root group *)

    val addToGroup : membership -> unit
    val addToGroups : int * int list -> unit
    val removeFromGroup : membership -> unit
    val groupMembers : int -> Init.user list

    val inGroupNum : int -> bool
    val inGroupName : string -> bool
    val reallyInGroupName : string -> bool

    (* These raise Access if the check fails *)
    val requireGroupNum : int -> unit
    val requireGroupName : string -> unit
    val requireAnyGroupName : string list -> unit
end
