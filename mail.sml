structure Mail :> MAIL =
struct

fun writeToLog s = ()
    (*let
	val outf = TextIO.openAppend (Init.scratchDir ^ "/log/mail.log")
    in
	TextIO.output (outf, s);
	TextIO.closeOut outf
    end*)

type session = (TextIO.instream, TextIO.outstream) Unix.proc

fun mopen () = Unix.execute ("/usr/sbin/sendmail", ["-t"])
	       before writeToLog "\nNEW MESSAGE\n"

fun mwrite (ses, s) = (TextIO.output (Unix.textOutstreamOf ses, s);
		       writeToLog s)

fun mclose ses = Unix.reap ses

(*type session = unit

fun mopen () = ()

fun mwrite ((), s) = print s

fun mclose () = OS.Process.success*)

end
