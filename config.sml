structure Config :> CONFIG = struct

val scratchDir = "/afs/hcoop.net/user/h/hc/hcoop"
val urlPrefix = "https://members.hcoop.net/portal/"
val emailSuffix = "@hcoop.net"
val boardEmail = "board" ^ emailSuffix

val dbstring = "dbname='hcoop_portal3' user='hcoop' host='postgres' port=5433"

val kerberosSuffix = "@HCOOP.NET"
		     
val passgenDbstring = "dbname='hcoop_passgen' user='hcoop' host='postgres' port=5433"

val statsRoot = "/afs/hcoop.net/user/h/hc/hcoop/portal-tools/etc/stats/"
val staticFilesRoot = "/afs/hcoop.net/user/h/hc/hcoop/portal-tools/etc/static/"

val joinBannedEmailDomains = [ "163.com", "yeah.net", "inbox.ru"]

end
