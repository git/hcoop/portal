structure Pass :> PASS =
struct

fun validEmail email =
    case String.fields (fn ch => ch = #"@") email of
	[_, dom] => dom <> "localhost"
      | _ => false

fun change (email, old, new) =
    let
	val [user, dom] = String.fields (fn ch => ch = #"@") email
	val proc = Unix.executeInEnv ("/usr/local/sbin/domtool-portal", ["vmailpasswd", dom, user],
				      ["DOMTOOL_USER=hcoop.daemon"])
	val out = Unix.textOutstreamOf proc

	fun eatinput inf =
	    case TextIO.inputLine inf of
		NONE => ()
	      | SOME line => eatinput inf
    in
	TextIO.output (out, old ^ "\n");
	TextIO.output (out, new ^ "\n");
	TextIO.flushOut out;

	eatinput (Unix.textInstreamOf proc);
	TextIO.closeOut out;

	OS.Process.isSuccess (Unix.reap proc)
    end

end
