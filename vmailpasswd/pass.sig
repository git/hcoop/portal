signature PASS =
sig
    val validEmail : string -> bool
    val change : string * string * string -> bool
end
