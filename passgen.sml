structure PassGen :> PASSGEN = struct

open Sql Init

fun lookup id =
    let
	val db = C.conn Config.passgenDbstring
    in
	(case C.oneOrNoRows db ($`SELECT pass FROM Pass WHERE id = ^(C.intToSql id)`) of
	     NONE => NONE
	   | SOME [pass] => SOME (C.stringFromSql pass)
	   | _ => raise Fail "Bad \"SELECT pass\" row")
	before C.close db
    end

end
