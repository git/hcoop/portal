signature APP =
sig
    datatype status =
	     CONFIRMING
	   | PENDING
	   | ACCEPTED
	   | REJECTED
	   | ADDED
	   | BEING_ADDED

    val readTosBody : unit -> string
    val readTosAgree : unit -> string
    val readTosMinorAgree : unit -> string

    type app = { id : int, name : string, rname : string, gname : string option, email : string,
		 forward : bool, uses : string, other : string,
		 passwd : string, status : status, applied : Init.C.timestamp,
		 ipaddr : string option,
		 confirmed : Init.C.timestamp option, decided : Init.C.timestamp option,
		 msg : string, unix_passwd : string,
		 paypal : string option, checkout : string option }

    val lookupApp : int -> app
    val listApps : status list -> app list

    val votes : int -> (int * string) list
    val vote : int * int -> unit
    val unvote : int * int -> unit

    val deny : int * string -> bool
    val approve : int * string -> bool
    val preAdd : int -> unit
    val add : int -> unit
    val abortAdd : int -> unit
    val welcome : int -> unit

    val searchPaypal : string -> app list
    val searchCheckout : string -> app list
end
