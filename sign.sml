structure Sign = Request(struct
			 val table = "Sign"
			 val adminGroup = "server"
			 fun subject _ = "SSL certificate signing request"
			 val template = "sign"
			 val descr = "SSL certificate signing"
				      
			 fun body (mail, data) =
			     (Mail.mwrite (mail, " Request: ");
			      Mail.mwrite (mail, data);
			      Mail.mwrite (mail, "\n"))
			 end)
