<% val you = Init.getUserId ();
val yourname = Init.getUserName ();

val nodeNum = case $"node" of
		  "" => 6
		| node => Web.stoi node;
val nodeName = Init.nodeName nodeNum;

val uname = case $"uname" of
	  "" => yourname
	| uname => uname;

val socks = Sec.socketPerms {node = nodeNum, uname = uname};
val tpe = Sec.isTpe {node = nodeNum, uname = uname};
val cron = Sec.cronAllowed {node = nodeNum, uname = uname};

ref showNormal = true;

@header [("title", ["Security settings"])];

if $"cmd" = "socks" then
	showNormal := false;
	val socks = $"socks";
	%>Are you sure you want to request that socket permissions for <b><% Web.html uname %></b> on <b><% Web.html nodeName %></b> be changed to <b><% Web.html socks %></b>?<br>
	<a href="sec?cmd=socks2&node=<% nodeNum %>&uname=<% Web.urlEncode uname %>&socks=<% Web.urlEncode socks %>&msg=<% Web.urlEncode ($"msg") %>">Yes, place the request!</a><%
elseif $"cmd" = "socks2" then
	val id = Sec.Req.add {usr = you, node = nodeNum, data = String.concat [uname, ": change socket permissions to ", $"socks"], msg = $"msg"};
	if not (Sec.Req.notifyNew id) then
		%><h3>Error sending e-mail notification</h3><%
	end
	%><h3>Request added</h3><%

elseif $"cmd" = "tpe" then
	showNormal := false;
	val tpe = iff $"tpe" = "yes" then "on" else "off";
	%>Are you sure you want to request that trusted-path-executables-only for <b><% Web.html uname %></b> on <b><% Web.html nodeName %></b> be turned <b><% tpe %></b>?<br>
	<a href="sec?cmd=tpe2&node=<% nodeNum %>&uname=<% Web.urlEncode uname %>&tpe=<% tpe %>&msg=<% Web.urlEncode ($"msg") %>">Yes, place the request!</a><%
elseif $"cmd" = "tpe2" then
	val id = Sec.Req.add {usr = you, node = nodeNum, data = String.concat [uname, ": turn tpe ", $"tpe"], msg = $"msg"};
	if not (Sec.Req.notifyNew id) then
		%><h3>Error sending e-mail notification</h3><%
	end
	%><h3>Request added</h3><%

elseif $"cmd" = "cron" then
	showNormal := false;
	val cron = iff $"cron" = "yes" then "enabled" else "disabled";
	%>Are you sure you want to request that <tt>cron</tt> permissions for <b><% Web.html uname %></b> on <b><% Web.html nodeName %></b> be <b><% cron %></b>?<br>
	<a href="sec?cmd=cron2&node=<% nodeNum %>&uname=<% Web.urlEncode uname %>&cron=<% cron %>&msg=<% Web.urlEncode ($"msg") %>">Yes, place the request!</a><%
elseif $"cmd" = "cron2" then
	val cron = iff $"cron" = "enabled" then "enable" else "disable";
	val id = Sec.Req.add {usr = you, node = nodeNum, data = String.concat [uname, ": ", cron, " cron access"], msg = $"msg"};
	if not (Sec.Req.notifyNew id) then
		%><h3>Error sending e-mail notification</h3><%
	end
	%><h3>Request added</h3><%
elseif $"cmd" = "rule" then
	showNormal := false;
	val rule = $"rule";

	if Sec.validRule rule then
		%>Are you sure you want to request the firewall rule <b><% Web.html uname %>&nbsp;<% Web.html rule %></b> on <b><% Web.html nodeName %></b>?<br>
	<a href="sec?cmd=rule2&node=<% nodeNum %>&uname=<% Web.urlEncode uname %>&rule=<% Web.urlEncode rule %>&msg=<% Web.urlEncode ($"msg") %>">Yes, place the request!</a><%
	else
		%>"<% Web.html rule %>" is not a valid firewall rule! Please reread <a href="http://wiki.hcoop.net/wiki/FirewallRules">the instructions</a>, and remember to leave off the initial username portion.<%
	end

elseif $"cmd" = "rule2" then
	val rule = $"rule";

	if Sec.validRule rule then
		val id = Sec.Req.add {usr = you, node = nodeNum, data = String.concat ["Add firewall rule \"", nodeName, " ", uname, " ", rule, "\""], msg = $"msg"};
		if not (Sec.Req.notifyNew id) then
			%><h3>Error sending e-mail notification</h3><%
		end
		%><h3>Request added</h3><%
	else
		%>"<% Web.html rule %>" is not a valid firewall rule! Please reread <a href="http://wiki.hcoop.net/wiki/FirewallRules">the instructions</a>, and remember to leave off the initial username portion.<%
	end

elseif $"modRule" <> "" then
	showNormal := false;
	val oldRule = $"modRule";
	val rule = $"rule"
	if oldRule = rule then
		%>You didn't modify the textbox for this rule before clicking the button, so there is no request to be made.<%
	else
		%>Are you sure you want to request that firewall rule <b><% Web.html uname %>&nbsp;<% Web.html oldRule %></b> be replaced by <b><% Web.html uname %>&nbsp;<% Web.html rule %></b> on <b><% Web.html nodeName %></b>?<br>
		<a href="sec?node=<% nodeNum %>&uname=<% Web.urlEncode uname %>&modRule2=<% Web.urlEncode oldRule %>&rule=<% Web.urlEncode rule %>&msg=<% Web.urlEncode ($"msg") %>">Yes, place the request!</a><%
	end
elseif $"modRule2" <> "" then
	val id = Sec.Req.add {usr = you, node = nodeNum, data = String.concat ["Change firewall rule \"", uname, " ", $"modRule2", "\" to \"", uname, " ", $"rule", "\""], msg = $"msg"};
	if not (Sec.Req.notifyNew id) then
		%><h3>Error sending e-mail notification</h3><%
	end
	%><h3>Request added</h3><%

elseif $"delRule" <> "" then
	showNormal := false;
	val oldRule = $"delRule";
	%>Are you sure you want to request that firewall rule <b><% Web.html uname %>&nbsp;<% Web.html oldRule %></b> on <b><% Web.html nodeName %></b> be <b>deleted</bD>?<br>
	<a href="sec?node=<% nodeNum %>&uname=<% Web.urlEncode uname %>&delRule2=<% Web.urlEncode oldRule %>&msg=<% Web.urlEncode ($"msg") %>">Yes, place the request!</a><%
elseif $"delRule2" <> "" then
	val id = Sec.Req.add {usr = you, node = nodeNum, data = String.concat ["Delete firewall rule \"", uname, " ", $"delRule2", "\""], msg = $"msg"};
	if not (Sec.Req.notifyNew id) then
		%><h3>Error sending e-mail notification</h3><%
	end
	%><h3>Request added</h3><%

elseif $"cmd" = "open" then
	showNormal := false;
	Group.requireGroupName "server";
	%><h3>Open requests</h3>
	<a href="sec?cmd=list">List all requests</a><%

	foreach (name, req) in Sec.Req.listOpen () do %>
<br><hr><br>
<table class="blanks">
<tr> <td>By:</td> <td><a href="user?id=<% #usr req %>"><% name %></a></td> </tr>
<tr> <td>Time:</td> <td><% #stamp req %> (<% Util.diffFromNow (#stamp req) %> ago)</td></tr>
<tr> <td>Node:</td> <td><% Web.html (Init.nodeName (#node req)) %></td> </tr>
<tr> <td>Request:</td> <td><% #data req %></td> </tr>
<tr> <td>Msg:</td> <td colspan="2"><% Web.html (#msg req) %></td> </tr>
</table>

<br>
<a href="sec?mod=<% #id req %>">[Modify]</a>
<a href="sec?del=<% #id req %>">[Delete]</a><br>

<%	end

elseif $"cmd" = "list" then
	showNormal := false;
	Group.requireGroupName "server"
	%><h3>All requests</h3><%

	foreach (name, req) in Sec.Req.list () do %>
<br><hr><br>
<table class="blanks">
<tr> <td>By:</td> <td colspan="2"><a href="user?id=<% #usr req %>"><% name %></a></td> </tr>
<tr> <td>Time:</td> <td colspan="2"><% #stamp req %> (<% Util.diffFromNow (#stamp req) %> ago)</td></tr>
<tr> <td>Node:</td> <td><% Web.html (Init.nodeName (#node req)) %></td> </tr>
<tr> <td>Request:</td> <td><% #data req %></td> </tr>
<tr> <td>Reason:</td> <td colspan="2"><% Web.html (#msg req) %></td> </tr>
</table>

<br>
<a href="sec?mod=<% #id req %>">[Modify]</a>
<a href="sec?del=<% #id req %>">[Delete]</a>

<%	end

elseif $"mod" <> "" then
	showNormal := false;
	Group.requireGroupName "server";
	val id = Web.stoi ($"mod");
	val req = Sec.Req.lookup id;
	val user = Init.lookupUser (#usr req) %>
<h3>Handle request</h3>

<form action="sec" method="post">
<input type="hidden" name="save" value="<% id %>">
<table class="blanks">
<tr> <td>Requestor:</td> <td><a href="user?id=<% #usr req %>"><% #name user %></a></td> </tr>
<tr> <td>Time:</td> <td><% #stamp req %> (<% Util.diffFromNow (#stamp req) %> ago)</td></tr>
<tr> <td>Status:</td> <td><select name="status">
	<option value="0"<% if #status req = Sec.Req.NEW then %> selected<% end %>>New</option>
	<option value="1"<% if #status req = Sec.Req.INSTALLED then %> selected<% end %>>Installed</option>
	<option value="2"<% if #status req = Sec.Req.REJECTED then %> selected<% end %>>Rejected</option>
</select></td> </tr>
<tr> <td>Node:</td> <td><select name="node">
<% foreach node in Init.listNodes () do %>
	<option value="<% #id node %>"<% if #id node = #node req then %> selected<% end %>><% Web.html (#name node) %> (<% Web.html (#descr node) %>)</option>
<% end %></select></td> </tr>
<tr> <td>Request:</td> <td><input name="req" value="<% Web.html (#data req) %>"></td> </tr>
<tr> <td>Message:</td> <td><textarea name="msg" rows="10" cols="80" wrap="soft"><% Web.html (#msg req) %></textarea></td> </tr>
<tr> <td><input type="submit" value="Save"></td> </tr>
</table>
</form>

<% elseif $"save" <> "" then
	showNormal := false;
	Group.requireGroupName "server";
	val id = Web.stoi ($"save");
	val req = Sec.Req.lookup id;
	val oldStatus = #status req;
	val newStatus = Sec.Req.statusFromInt (Web.stoi ($"status"));
	Sec.Req.modify {req with node = nodeNum, data = $"req", msg = $"msg", status = newStatus};
	if not (Sec.Req.notifyMod {old = oldStatus, new = newStatus, changer = Init.getUserName(), req = id}) then
		%><h3>Error sending e-mail notification</h3><%
	end
	%><h3>Request modified</h3>
	Back to: <a href="sec?cmd=open">open requests</a>, <a href="sec?cmd=list">all requests</a>

<% elseif $"del" <> "" then
	showNormal := false;
	Group.requireGroupName "server";
	val id = Web.stoi ($"del");
	val req = Sec.Req.lookup id;
	val user = Init.lookupUser (#usr req)
	%><h3>Are you sure you want to delete request by <% #name user %> for "<% #data req %>" on <% Web.html (Init.nodeName (#node req)) %>?</h3>
	<a href="sec?del2=<% id %>">Yes, I'm sure!</a>

<% elseif $"del2" <> "" then
	showNormal := false;
	Group.requireGroupName "server";
	val id = Web.stoi ($"del2");
	Sec.Req.delete id
	%><h3>Request deleted</b><h3>
	Back to: <a href="sec?cmd=open">open requests</a>, <a href="sec?cmd=list">all requests</a>

<% end;

if showNormal then
  @secnormal [("uname", [uname]),
              ("nodeNum", [Int.toString nodeNum])];
end %>

<% @footer[] %>