structure ChooseDomain :> CHOOSE_DOMAIN = struct

val domains = Domtool.perms "domain"

fun yourDomain {user, domain} = Domtool.hasPerm "domain" user domain

end
