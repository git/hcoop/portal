<% @header[("title", ["Member locations"])];

val admin = Group.inGroupName "location";

ref showNormal = true;

if $"cmd" = "add" then
	val parent = (case $"parent" of "" => NONE | parent => SOME (Web.stoi parent));
	val name = $"name";

	if Location.alreadyExists (parent, name) then
		%><h3>That location already exists!</h3><%
	else
		val id = Location.addLocation (parent, $"name")
		%><h3>Location added</h3>
<%	end

elseif $"mod" <> "" then
	Group.requireGroupName "location";
	showNormal := false;
	val id = Web.stoi ($"mod");
	val loc = Location.lookupLocation id %>

<h3>Modify a location</h3>

<form action="location" method="post">
<input type="hidden" name="save" value="<% id %>">
<table class="blanks">
<tr> <td>Parent:</td> <td><select name="parent">
	<option value=""<% if #parent loc = NONE then %> selected<% end %>>&lt;None&gt;</option>
<% ref indent = 0;
foreach item in Location.locationTree (NONE, NONE) do
	switch item of
		  Util.BEGIN =>
			indent := indent + 1 %>
			<ul>
<%		| Util.END =>
			indent := indent - 1 %>
			</ul>
<%		| Util.ITEM loc2 => %>
			<option value="<% #id loc2 %>"<% if SOME (#id loc2) = #parent loc then %> selected<% end %>><% for i in 1 .. indent do %>-<% end %><% Web.html (#name loc2) %></option>
<%	end
end %>
</select></td> </tr>
<tr> <td>Name:</td> <td><input name="name" required="required" value="<% Web.html (#name loc) %>"></td> </tr>
<tr> <td><input type="submit" value="Save"></td> </tr>
</table>
</form>

<% elseif $"save" <> "" then
	Group.requireGroupName "location";
	val id = Web.stoi ($"save");
	val loc = Location.lookupLocation id;
	Location.modLocation {loc with parent = (case $"parent" of "" => NONE | parent => SOME (Web.stoi parent)),
		name = $"name"};
	%><h3>Location saved</h3>	

<% elseif $"del" <> "" then
	Group.requireGroupName "location";
	showNormal := false;
	val id = Web.stoi ($"del");
	val loc = Location.lookupLocation id %>
	<h3>Are you sure you want to delete "<% Web.html (#name loc) %>"?</h3>
	<a href="location?del2=<% id %>">Yes, delete "<% Web.html (#name loc) %>"!</a>

<% elseif $"del2" <> "" then
	Group.requireGroupName "location";
	val id = Web.stoi ($"del2");
	val loc = Location.lookupLocation id;
	Location.deleteLocation id %>
	<h3>Deleted location "<% Web.html (#name loc) %>"</h3>

<% elseif $"addLoc" <> "" then
	Location.addToLocation {loc = Web.stoi ($"addLoc"), usr = Init.getUserId ()}
	%><h3>Added</h3>

<% elseif $"remLoc" <> "" then
	Location.removeFromLocation {loc = Web.stoi ($"remLoc"), usr = Init.getUserId ()}
	%><h3>Removed</h3>

<% elseif $"id" <> "" then
	showNormal := false;
	val id = Web.stoi ($"id");
	val loc = Location.lookupLocation id %>

<h2><b><% Web.html (#name loc) %></b></h2>

<% switch #parent loc of
	SOME par =>
		val ploc = Location.lookupLocation par;
		%><b>Parent</b>: <a href="location?id=<% par %>"><% Web.html (#name ploc) %></a><%
end %>

<h3>Residents:</h3>
<% ref first = true;
foreach user in Location.residents id do
	if first then
		first := false
	else
		%>, <%
	end;
	%><a href="user?id=<% #id user %>"><% #name user %></a><%
end %>

<h3>Regions:</h3>
<% foreach loc in Location.subLocations (SOME id) do %>
	<a href="location?id=<% #id loc %>"><% Web.html (#name loc) %></a><br>
<% end;

end; %>


<h3>Add a new location</h3>

<form action="location" method="post">
<input type="hidden" name="cmd" value="add">
<table class="blanks">
<tr> <td>Parent:</td> <td><select name="parent">
	<option value="">&lt;None&gt;</option>
<% ref indent = 0;
foreach item in Location.locationTree (NONE, NONE) do
	switch item of
		  Util.BEGIN =>
			indent := indent + 1 %>
			<ul>
<%		| Util.END =>
			indent := indent - 1 %>
			</ul>
<%		| Util.ITEM loc => %>
			<option value="<% #id loc %>"><% for i in 1 .. indent do %>-<% end %><% Web.html (#name loc) %></option>
<%	end
end %>
</select></td> </tr>
<tr> <td>Name:</td> <td><input name="name" required="required"></td> </tr>
<tr> <td><input type="submit" value="Add"></td> </tr>
</table>
</form>

<% val withUser = Location.locationTreeWithUser (NONE, NONE, Init.getUserId ()) %>

<h3>Add yourself to a location</h3>

Adding yourself to a location automatically adds you to all more general loations.

<form action="location" method="post">
<select name="addLoc">
<% ref indent = 0;
foreach item in withUser do
	switch item of
		  Util.BEGIN =>
			indent := indent + 1 %>
			<ul>
<%		| Util.END =>
			indent := indent - 1 %>
			</ul>
<%		| Util.ITEM (true, _) =>
		| Util.ITEM (false, loc) => %>
			<option value="<% #id loc %>"><% for i in 1 .. indent do %>-<% end %><% Web.html (#name loc) %></option>
<%	end
end %>
</select> <input type="submit" value="Add">
</form>

<h3>Remove yourself from a location</h3>

<form action="location" method="post">
<select name="remLoc">
<% ref indent = 0;
foreach item in withUser do
	switch item of
		  Util.BEGIN =>
			indent := indent + 1 %>
			<ul>
<%		| Util.END =>
			indent := indent - 1 %>
			</ul>
<%		| Util.ITEM (false, _) =>
		| Util.ITEM (true, loc) => %>
			<option value="<% #id loc %>"><% for i in 1 .. indent do %>-<% end %><% Web.html (#name loc) %></option>
<%	end
end %>
</select> <input type="submit" value="Remove">
</form>


<% if showNormal then %>
<ul>
<% foreach item in Location.locationTreeWithCounts (NONE, NONE) do
	switch item of
		  Util.BEGIN => %>
			<ul>
<%		| Util.END => %>
			</ul>
<%		| Util.ITEM (num, loc) => %>
			<li> <a href="location?id=<% #id loc %>"><% Web.html (#name loc) %></a> (<% num %>)
<a href="location?mod=<% #id loc %>">[Modify]</a> <a href="location?del=<% #id loc %>">[Delete]</a></li>
<%	end
end %>
</ul>



<% end %>

<% @footer[] %>