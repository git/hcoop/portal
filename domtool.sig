signature DOMTOOL = sig
    val perms : string -> string -> string list
    val hasPerm : string -> string -> string -> bool
end
