signature BALANCE =
sig
    type balance = {id : int, name : string, amount : real}

    val addBalance : string -> int
    val lookupBalance : int -> balance
    val modBalance : balance -> unit
    val deleteBalance : int -> unit
    val listBalances : unit -> balance list
    val listOwnedBalances : unit -> balance list
    val listUnownedBalances : unit -> balance list
    val listBalanceUsers : int -> Init.user list
    val listNegativeOwnedBalances : unit -> balance list

    val validBalanceName : string -> bool
    val balanceNameToId : string -> int option

    val sumOwnedBalances : unit -> real
    val isNegative : balance -> bool

    val depositAmount : int -> real
end
