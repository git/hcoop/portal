signature MAIL =
sig
    type session
    val mopen : unit -> session
    val mwrite : session * string -> unit
    val mclose : session -> OS.Process.status
end
				       