signature ROLL =
sig
    val activeUsernames : unit -> string list

    type roll_call = {
	 id : int,
	 title : string,
	 msg : string,
	 started : Init.C.timestamp
    }

    val addRollCall : string * string -> int
    val modRollCall : roll_call -> unit
    val deleteRollCall : int -> unit
    val lookupRollCall : int -> roll_call
    val listRollCalls : unit -> roll_call list

    type roll_call_entry = {
	 rol : int,
	 usr : int,
	 code : string,
	 responded : Init.C.timestamp option
    }

    val listEntries : int -> (Init.user * roll_call_entry) list * (Init.user * roll_call_entry) list
    val lookupEntry : int * int -> roll_call_entry
    val respond : int * int -> unit
end
