structure Domain = Request(struct
			   val table = "Domain"
			   val adminGroup = "server"
			   fun subject dom = "Domain set-up request: " ^ dom
			   val template = "domain"
			   val descr = "domain"
			   fun body (mail, dom) =
			       (Mail.mwrite (mail, dom);
				Mail.mwrite (mail, "\n"))
			   end)
