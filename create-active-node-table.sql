BEGIN;

CREATE TABLE ActiveWebNode
(
	id integer primary key,
	foreign key (id) references WebNode
);

INSERT INTO ActiveWebNode (id) VALUES (3);
INSERT INTO ActiveWebNode (id) VALUES (4);

COMMIT;
