signature SUPPORT =
sig
    datatype status =
	     NEW
	   | PENDING
	   | CLOSED

    type category = { id : int, grp : int, name : string, descr : string }
    type issue = { id : int, usr : int, cat : int, title : string, priv : bool, status : status,
		   stamp : Init.C.timestamp, pstamp : Init.C.timestamp option, cstamp : Init.C.timestamp option }
    type post = { id : int, usr : int, iss : int, body : string, stamp : Init.C.timestamp }
    type subscription = { usr : int, cat : int }

    val lookupCategory : int -> category
    val listCategories : unit -> category list
    val listCategoriesWithSubscriptions : int -> (bool * category) list
    val addCategory : int * string * string -> int
    val modCategory : category -> unit
    val deleteCategory : int -> unit

    val lookupIssue : int -> issue
    val listIssues : unit -> issue list
    val listOpenIssues : int -> (string * issue) list
    val listCategoryIssues : int -> (string * issue) list
    val listOpenCategoryIssues : int * int -> (string * issue) list
    val listOpenCategoryIssuesAdmin : int -> (string * issue) list
    val addIssue : int * int * string * bool * status -> int
    val modIssue : issue -> unit
    val deleteIssue : int -> unit

    val lookupPost : int -> post
    val listPosts : int -> (string * post) list
    val addPost : int * int * string -> int
    val modPost : post -> unit
    val deletePost : int -> unit

    val subscribed : subscription -> bool
    val subscribe : subscription -> unit
    val unsubscribe : subscription -> unit
				      
    val validTitle : string -> bool
    val allowedToSee : int -> bool
    val allowedToEdit : int -> bool
			       
    val notifyCreation : int -> bool
    val notifyPost : int -> bool
    val notifyStatus : int * status * status * int -> bool
end