structure Main = struct

structure C = PgClient

fun main _ =
    let
	val db = C.conn "dbname='hcoop_hcoop' host='postgres'"

	fun printOne [name] = print (C.stringFromSql name ^ "@hcoop.net\n")
    in
	C.app db printOne "SELECT name FROM WebUser";
	C.close db;
	OS.Process.success
    end
end
