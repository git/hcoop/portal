signature APP =
sig
    structure C : SQL_CLIENT

    val init : unit -> unit
    val done : unit -> unit

    val readTosBody : unit -> string
    val readTosAgree : unit -> string
    val readTosMinorAgree : unit -> string

    type application = { name : string, rname : string, gname : string option, email : string,
			 forward : bool, uses : string, other : string,
			 paypal : string option, checkout : string option }

    val apply : application -> string option

    val validEmail : string -> bool
    val validUsername : string -> bool
    val userExists : string -> bool

    val confirm : int * string -> bool

    val appUserName : int -> string
end
