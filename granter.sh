#!/bin/sh

grep "CREATE" tables.sql \
    | sed "s/^CREATE TABLE \(.*\)($/GRANT SELECT,UPDATE,INSERT,DELETE ON \1 TO \"$1\";/g" \
    | sed "s/^CREATE VIEW \(.*\)$/GRANT SELECT,UPDATE,INSERT,DELETE ON \1 TO \"$1\";/g" \
    | sed "s/^CREATE SEQUENCE \(.*\) START.*$/GRANT SELECT,UPDATE,INSERT,DELETE ON \1 TO \"$1\";/g"
