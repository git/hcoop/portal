signature CHOOSE_DOMAIN = sig
    val domains : string -> string list
    val yourDomain : {user : string, domain : string} -> bool
end
