signature APT_QUERY =
sig
    type info = { name : string, section : string, descr : string, installed : bool }

    val validName : string -> bool
    val query : {node : int, pkg : string} -> info option
end