signature SEC = sig
    structure Req : REQUESTH_OUT

    val findSubusers : string -> string list

    datatype socket_perms =
	     ANY
	   | CLIENT_ONLY
	   | SERVER_ONLY
	   | NADA

    val socketPerms : {node : int, uname : string} -> socket_perms
    val isTpe : {node : int, uname : string} -> bool
    val cronAllowed : {node : int, uname : string} -> bool

    val findFirewallRules : {node : int, uname : string} -> string list

    val validRule : string -> bool

    val fulldomain : string * string -> string
end
