<% @header [("title", ["Domain set-up requests"])];

val admin = Group.inGroupName "server";

if $"req" <> "" then
	val dom = $"req";
	if not (Util.validDomain dom) then
		%><h3>Invalid domain name</h3><%
	elseif String.isPrefix "www." dom and $"want_www" = "" then
	        val base = String.extract (dom, 4, NONE)
	        %><h3>Are you sure you want to request a domain that begins with "www."?</h3>
		<p>You probably meant to request <tt><% base %></tt> instead, which would grant you the ability to configure <tt><% dom %></tt> as well.  Unless you've registered <tt><% dom %></tt> directly with your registrar, you will be unable to configure it without the right to set nameservers for <tt><% base %></tt>.</p>
		<p><a href="?req=<% Web.urlEncode dom %>&want_www=1&msg=<% Web.urlEncode ($"msg") %>">Yes, go ahead and place the request!</a></p><%
	else
		val id = Domain.add (Init.getUserId(), dom, $"msg");
		if not (Domain.notifyNew id) then
			%><h3>Error sending e-mail notification</h3><%
		end
		%><h3>Request added</h3><%
	end

elseif $"cmd" = "open" then
	%><h3>Open requests</h3>
	<a href="domain?cmd=list">List all requests</a><%

	foreach (name, req) in Domain.listOpen () do %>
<br><hr><br>
<table class="blanks">
<tr> <td>By:</td> <td><a href="user?id=<% #usr req %>"><% name %></a></td> </tr>
<tr> <td>Time:</td> <td><% #stamp req %> (<% Util.diffFromNow (#stamp req) %> ago)</td> </tr>
<tr> <td>Domain:</td> <td><a href="<% Util.whoisUrl (#data req) %>"><% #data req %></a></td> </tr>
<tr> <td>Reason:</td> <td colspan="2"><% Web.html (#msg req) %></td> </tr>
</table>

<% if admin then %>
	<br>
	<a href="domain?mod=<% #id req %>">[Modify]</a>
	<a href="domain?del=<% #id req %>">[Delete]</a><br>
	To set up, run: <tt>domtool-admin grant <% name %> domain <% #data req %></tt>
<% end %>

<%	end

elseif $"cmd" = "list" then
	%><h3>All requests</h3><%

	foreach (name, req) in Domain.list () do %>
<br><hr><br>
<table class="blanks">
<tr> <td>By:</td> <td colspan="2"><a href="user?id=<% #usr req %>"><% name %></a></td> </tr>
<tr> <td>Time:</td> <td colspan="2"><% #stamp req %> (<% Util.diffFromNow (#stamp req) %> ago)</td> </tr>
<tr> <td>Domain:</td> <td><a href="<% Util.whoisUrl (#data req) %>"><% #data req %></a></td> </tr>
<tr> <td>Reason:</td> <td colspan="2"><% Web.html (#msg req) %></td> </tr>
</table>

<% if admin then %>
	<br>
	<a href="domain?mod=<% #id req %>">[Modify]</a>
	<a href="domain?del=<% #id req %>">[Delete]</a>
<% end %>

<%	end

elseif $"mod" <> "" then
	Group.requireGroupName "server";
	val id = Web.stoi ($"mod");
	val req = Domain.lookup id;
	val user = Init.lookupUser (#usr req) %>
<h3>Handle request</h3>

<form action="domain" method="post">
<input type="hidden" name="save" value="<% id %>">
<table class="blanks">
<tr> <td>Requestor:</td> <td><a href="user?id=<% #usr req %>"><% #name user %></a></td> </tr>
<tr> <td>Time:</td> <td><% #stamp req %> (<% Util.diffFromNow (#stamp req) %> ago)</td> </tr>
<tr> <td>Status</b>:</td> <td><select name="status">
	<option value="0"<% if #status req = Domain.NEW then %> selected<% end %>>New</option>
	<option value="1"<% if #status req = Domain.INSTALLED then %> selected<% end %>>Installed</option>
	<option value="2"<% if #status req = Domain.REJECTED then %> selected<% end %>>Rejected</option>
</select></td> </tr>
<tr> <td>Domain:</td> <td><input name="dom" value="<% #data req %>"></td> </tr>
<tr> <td>Message:</td> <td><textarea name="msg" rows="10" cols="80" wrap="soft"><% Web.html (#msg req) %></textarea></td> </tr>
<tr> <td><input type="submit" value="Save"></td> </tr>
</table>
</form>

<% elseif $"save" <> "" then
	Group.requireGroupName "server";
	val id = Web.stoi ($"save");
	val req = Domain.lookup id;
	val oldStatus = #status req;
	val newStatus = Domain.statusFromInt (Web.stoi ($"status"));
	Domain.modify {req with data = $"dom", msg = $"msg", status = newStatus};
	if not (Domain.notifyMod (oldStatus, newStatus, Init.getUserName(), id)) then
		%><h3>Error sending e-mail notification</h3><%
	end
	%><h3>Request modified</h3>
	Back to: <a href="domain?cmd=open">open requests</a>, <a href="domain?cmd=list">all requests</a>

<% elseif $"del" <> "" then
	Group.requireGroupName "server";
	val id = Web.stoi ($"del");
	val req = Domain.lookup id;
	val user = Init.lookupUser (#usr req)
	%><h3>Are you sure you want to delete request by <% #name user %> for <tt><% #data req %></tt>?</h3>
	<a href="domain?del2=<% id %>">Yes, I'm sure!</a>

<% elseif $"del2" <> "" then
	Group.requireGroupName "server";
	val id = Web.stoi ($"del2");
	Domain.delete id
	%><h3>Request deleted</b><h3>
	Back to: <a href="domain?cmd=open">open requests</a>, <a href="domain?cmd=list">all requests</a>

<% else %>

<h3>Request new domain</h3>

Enter the full Internet domain name that you own and would like set up here.  We don't do domain registration, so it is your responsibility to register this name with your registrar of choice before submitting it here.  The "Reason" field is optional.

<form action="domain" method="post">
<table class="blanks">
<tr> <td>Domain:</td> <td><input name="req" required="required"></td> </tr>
<tr> <td>Reason:</td> <td><textarea name="msg" rows="5" cols="80" wrap="soft"></textarea></td> </tr>
<tr> <td><input type="submit" value="Request"></td> </tr>
</table>
</form>

<h3>Your current domains</h3>

<p>To remove any domain you control, you can run <tt>domtool-admin
rmdom $DOMAIN</tt>.</p>

<ul>
<% foreach dom in ChooseDomain.domains (Init.getUserName ()) do %>
   <li><tt><% dom %></tt></li>
<% end %>
</ul>

<% end %>

<% @footer[] %>