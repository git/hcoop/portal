signature STATS =
sig
    type host = {ssl : bool,        (* Is it HTTPS? *)
		 hostname : string, (* Internet hostname *)
		 id : string}       (* Name of stats directory *)

    val getWebbw : int -> {total : int,
			   vhosts: {host : host, size : int} list,
			   users : {user : string, hosts : host list, size : int} list}
    (* Get web bandwidth usage stats.  The argument tells how many months ago
     * to look for the data.  The return gives the total b/w usage, a mapping from
     * vhosts to kilobytes, and a mapping from usernames to their vhosts and bandwidth
     * totals. *)

    val getWebbwUser : {user : string, last : int} -> {total : int,
						       vhosts : {host : host, size : int} list}

    type disk = {uname : string, (* UNIX username *)
		 kbs : int}      (* Number of kilobytes space used *)

    val getDiskUsage : unit -> disk list
    (* Get /home disk usage statistics, in descending blocks order. *)
end
