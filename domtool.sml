structure Domtool :> DOMTOOL = struct

fun perms class user =
    let
 	val proc = Unix.execute ("/bin/sh", ["-c", "DOMTOOL_USER=hcoop.daemon /usr/local/bin/domtool-admin perms " ^ user])
	val inf = Unix.textInstreamOf proc

	fun loop () =
	    case TextIO.inputLine inf of
		NONE => []
	      | SOME line =>
		case String.tokens (fn ch => ch = #":") line of
		    [class', permissions] => if class' = class then String.tokens Char.isSpace permissions else loop ()
		  | _ => loop ()
    in
	loop ()
	before ignore (Unix.reap proc)
    end

fun hasPerm class user value =
    List.exists (fn x => x = value) (perms class user)

end
