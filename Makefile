include Makefile.common

all: install-scripts install-static header.mlt footer.mlt

clean:
	rm header.mlt
	rm footer.mlt

header.mlt: $(prefix)/etc/hcoop.header header.setTitle header.mlt.in
	cat header.setTitle >header.mlt
	$(prefix)/bin/hcoop_header $(prefix) "<% Web.html title %>" >>header.mlt
	cat header.mlt.in >>header.mlt

footer.mlt: $(prefix)/etc/hcoop.footer footer.mlt.in
	cat footer.mlt.in >footer.mlt
	$(prefix)/bin/hcoop_footer $(prefix) >>footer.mlt

install-scripts: scripts/*
	install -m 750 -o hcoop scripts/hcoop_header $(prefix)/bin
	install -m 750 -o hcoop scripts/hcoop_footer $(prefix)/bin
	install -m 750 -o hcoop scripts/hcoop_html $(prefix)/bin

install-static: etc/*
	install -m 640 -o hcoop etc/tos.*.html $(prefix)/etc/static
	install -m 640 -o hcoop etc/hcoop.header $(prefix)/etc/
	install -m 640 -o hcoop etc/hcoop.footer $(prefix)/etc/
