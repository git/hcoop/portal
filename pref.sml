structure Pref :> PREF =
struct

open Util Sql Init

fun hasDirectory usr =
    case C.oneOrNoRows (getDb ()) ($`SELECT * FROM DirectoryPref WHERE usr = ^(C.intToSql usr)`) of
	NONE => false
      | _ => true

fun setDirectory usr =
    if hasDirectory usr then
	()
    else
	ignore (C.dml (getDb ()) ($`INSERT INTO DirectoryPref (usr) VALUES (^(C.intToSql usr))`))

fun unsetDirectory usr =
    ignore (C.dml (getDb ()) ($`DELETE FROM DirectoryPref WHERE usr = ^(C.intToSql usr)`))

fun subscribed (list, address) = OS.Process.isSuccess (OS.Process.system (String.concat
									 ["/usr/bin/sudo -u list /usr/local/sbin/portalsub ",
									  list,
									  " check ",
									  address]))

fun subscribe (list, address) = OS.Process.isSuccess (OS.Process.system (String.concat
									 ["/usr/bin/sudo -u list /usr/local/sbin/portalsub ",
									  list,
									  " add ",
									  address]))

fun unsubscribe (list, address) = OS.Process.isSuccess (OS.Process.system (String.concat
									       ["/usr/bin/sudo -u list /usr/local/sbin/portalsub ",
										list,
										" rm ",
										address]))

end
